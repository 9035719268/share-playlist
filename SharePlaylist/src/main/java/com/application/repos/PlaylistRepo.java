package com.application.repos;

import com.application.domain.Playlist;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlaylistRepo extends CrudRepository<Playlist, Integer> {
    List<Playlist> findByAuthor(String author);
}